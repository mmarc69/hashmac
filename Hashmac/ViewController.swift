//
//  ViewController.swift
//  Hashmac
//
//  Created by Marc MICHEL on 13/01/2017.
//  Copyright © 2017 Marc MICHEL. All rights reserved.
//

import Cocoa


class ViewController: NSViewController ,NSTextDelegate {

    var selectedType = "SHA1"
    var selectedInput = 0
    let pasteboard = NSPasteboard.general()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    

    
    @IBAction func typeSelect(_ sender: NSSegmentedControl) {
        
        
    }
    
    @IBAction func mdSelect(_ sender: NSSegmentedControl) {
       // Séléction d'un type de hash MD
        segShaType.selectedSegment = -1
        labelType.stringValue = segMdType.label(forSegment: segMdType.selectedSegment)! + " :"
        selectedType = segMdType.label(forSegment: segMdType.selectedSegment)!
    }

   
    @IBAction func shSelect(_ sender: NSSegmentedControl) {
        //Sélection d'un type de Hash SHA
        segMdType.selectedSegment = -1
        labelType.stringValue = segShaType.label(forSegment: segShaType.selectedSegment)! + " :"
        selectedType = segShaType.label(forSegment: segShaType.selectedSegment)!
    }
    
    
    
    //Récupérer le contenu du presse papier si c'est du texte
    @IBAction func pastHash(_ sender: NSButton) {
        if let nofElements = pasteboard.pasteboardItems?.count {
            if nofElements > 0 {
                
                // Assume they are strings
                var strArr: Array<String> = []
                for element in pasteboard.pasteboardItems! {
                    if let str = element.string(forType: "public.utf8-plain-text") {
                        strArr.append(str)
                    }
                }
                // Exit if no string was read
                if strArr.count == 0 { return }
                // Perform the paste operation
                let last = strArr.count - 1
                labelPast.stringValue = strArr[last]
            }
        }
    }
    
    //Envoi le contenu  du Hash généré vers le presse papier
    @IBAction func copyHash(_ sender: NSButton) {
        
        pasteboard.declareTypes([NSPasteboardTypeString], owner: nil)
        if labelHashGenere?.stringValue != nil {
            pasteboard.setString((labelHashGenere?.stringValue)!, forType: NSPasteboardTypeString)
        }
        
    }

    // Sélection de l'input Fichier /Texte
    @IBAction func selInput(_ sender: NSSegmentedControl) {
        switch btnInput.selectedSegment {
        case 0:
            
            boxTexte.isHidden = true
            labelPast.becomeFirstResponder()
        case 1:
            saisieTexte.stringValue = ""
            boxTexte.isHidden = false
            saisieTexte.becomeFirstResponder()
           
        default:
            break
        }
        
    }
    
    //Selection du Fichier en cliquant sur le bouton
    @IBAction func selectFileFromButton(_ sender: NSButton) {
        let dialog = NSOpenPanel();
        dialog.title                   = "Selctionnez un Fichier";
        dialog.showsResizeIndicator    = true;
        dialog.showsHiddenFiles        = false;
        dialog.canChooseDirectories    = false;
        dialog.canCreateDirectories    = false;
        dialog.allowsMultipleSelection = false;
        //dialog.allowedFileTypes        = [];
        
        if (dialog.runModal() == NSModalResponseOK) {
            let result = dialog.urls // Pathname of the file
            
            if (result.count != 0) {
                let fichier = result.first
                print(fichier?.path ?? "")
                
                
            }
        } else {
            // User clicked on "Cancel"
            return
        }
        // appel de la fonction de calcul du hash
        
        
        
    }
    
    
    class DropView: NSView {
        
        var filePath: String?
        let expectedExt = ["kext"]  //file extensions allowed for Drag&Drop
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
            
            self.wantsLayer = true
            self.layer?.backgroundColor = NSColor.gray.cgColor
            
            register(forDraggedTypes: [NSFilenamesPboardType, NSURLPboardType])
        }
        
        override func draw(_ dirtyRect: NSRect) {
            super.draw(dirtyRect)
            // Drawing code here.
        }
        
        override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
            if checkExtension(sender) == true {
                self.layer?.backgroundColor = NSColor.blue.cgColor
                return .copy
            } else {
                return NSDragOperation()
            }
        }
        
        fileprivate func checkExtension(_ drag: NSDraggingInfo) -> Bool {
            guard let board = drag.draggingPasteboard().propertyList(forType: "NSFilenamesPboardType") as? NSArray,
                let path = board[0] as? String
                else { return false }
            
            let suffix = URL(fileURLWithPath: path).pathExtension
            for ext in self.expectedExt {
                if ext.lowercased() == suffix {
                    return true
                }
            }
            return false
        }
        
        override func draggingExited(_ sender: NSDraggingInfo?) {
            self.layer?.backgroundColor = NSColor.gray.cgColor
        }
        
        override func draggingEnded(_ sender: NSDraggingInfo?) {
            self.layer?.backgroundColor = NSColor.gray.cgColor
        }
        
        override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
            guard let pasteboard = sender.draggingPasteboard().propertyList(forType: "NSFilenamesPboardType") as? NSArray,
                let path = pasteboard[0] as? String
                else { return false }
            
            //GET YOUR FILE PATH !!
            self.filePath = path
            Swift.print("FilePath: \(filePath)")
            
            return true
        }
    }
    
    

    
    @IBOutlet weak var segInputChoice: NSSegmentedControl!
    @IBOutlet weak var segMdType: NSSegmentedControl!
    @IBOutlet weak var segShaType: NSSegmentedControl!
    @IBOutlet weak var labelType: NSTextField!
    @IBOutlet weak var labelPast: NSTextField!
    @IBOutlet weak var labelHashGenere: NSTextField?
    @IBOutlet weak var dropFIle: NSImageView!
    @IBOutlet weak var saisieTexte: NSTextField!
    @IBOutlet weak var btnInput: NSSegmentedControl!
    @IBOutlet weak var btnSelectFile: NSButton!
    @IBOutlet weak var boxTexte: NSBox!
    @IBOutlet weak var boxDrop: NSBox!
    
}

